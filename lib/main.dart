import 'package:flutter/material.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/pages/home_page.dart';
import 'package:my_movies_list/ui/pages/login_page.dart';
import 'package:my_movies_list/ui/pages/rated_title_list_page.dart';
import 'package:my_movies_list/ui/pages/register_account_page.dart';
import 'package:my_movies_list/ui/pages/search_page.dart';
import 'package:my_movies_list/ui/pages/splash_page.dart';
import 'package:my_movies_list/ui/pages/title_details_page.dart';
import 'package:my_movies_list/ui/pages/user_list_page.dart';

void main() {
  setUpLocator();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(primarySwatch: Colors.blue),
      initialRoute: SplashPage.name,
      routes: {
        LoginPage.name: (_) => const LoginPage(),
        HomePage.name: (_) => const HomePage(),
        SearchPage.name: (_) => SearchPage(),
        TitleDetailsPage.name: (_) => const TitleDetailsPage(),
        RegisterAccountPage.name: (_) => const RegisterAccountPage(),
        RatedTitleListPage.name: (_) => const RatedTitleListPage(),
        UserListPage.name: (_) => UserListPage(),
        SplashPage.name: (_) => const SplashPage(),
      },
    );
  }
}
