import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:my_movies_list/data/models/title_model.dart';
import 'package:my_movies_list/data/repositories/title_repository_interface.dart';

abstract class SearchState {}

class InitialSearchState implements SearchState {}

class ProcessingSearchState implements SearchState {}

class SuccessSearchState implements SearchState {
  List<TitleModel> titles;

  SuccessSearchState(this.titles);
}

class SearchCubit extends Cubit<SearchState> {
  final TitleRepositoryInterface _repository;

  final List<TitleModel> _list = [];
  int _page = 0;
  int _count = 0;
  String _previousName = '';

  SearchCubit(this._repository) : super(InitialSearchState());

  void clearSession() {
    _page = 0;
    _count = 0;
    _list.clear();
  }

  Future<void> getMovieList(String name) async {
    if (name.isEmpty) {
      clearSession();
      emit(InitialSearchState());
    } else {
      if (name != _previousName) {
        clearSession();
        _previousName = name;
      }

      if (_count == 0) {
        _count = await _repository.getCountMovieList(params: {'name': name});
      }

      if (_page == 0) {
        emit(ProcessingSearchState());
      }

      if (_list.length < _count) {
        _page++;
        var response = await _repository.getMovieList(params: {'name': name, 'page': _page});

        _list.addAll(response);
        emit(SuccessSearchState(_list));
      }
    }
  }
}
