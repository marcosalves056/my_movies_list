import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/data/exceptions/title_not_rated.dart';
import 'package:my_movies_list/data/repositories/title_repository_interface.dart';

abstract class TitleRateState {}

class ProcessingTitleRateState extends TitleRateState {}

class SuccessTitleRateState extends TitleRateState {
  final bool? rate;
  final int titleId;

  SuccessTitleRateState(this.rate, this.titleId);
}

class TitleNotRatedExceptionTitleRateState extends TitleRateState {}

class TitleRateCubit extends Cubit<TitleRateState> {
  final TitleRepositoryInterface _repository;

  TitleRateCubit(this._repository) : super(ProcessingTitleRateState());

  Future<void> getTitleRate(int titleId, {bool isTvShow = false}) async {
    emit(ProcessingTitleRateState());

    try {
      var rate = await _repository.getTitleRate(titleId, isTvShow: isTvShow);
      emit(SuccessTitleRateState(rate == 1, titleId));
    } on TitleNotRatedException {
      emit(TitleNotRatedExceptionTitleRateState());
    }
  }

  Future<void> saveTitleRate(int titleId, int rate, {bool isTvShow = false}) async {
    // emit(ProcessingTitleRateState());

    await _repository.saveTitleRate(titleId, rate, isTvShow: isTvShow);
    emit(SuccessTitleRateState(rate == 1, titleId));
  }
}
