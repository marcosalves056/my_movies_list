import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:my_movies_list/data/models/user_model.dart';
import 'package:my_movies_list/data/repositories/user_repository_interface.dart';

abstract class UserListState {}

class ProcessingUserListState implements UserListState {}

class SuccessUserListState implements UserListState {
  List<UserModel> users;

  SuccessUserListState(this.users);

  List<UserModel> get props => users;
}

class FailUserListState implements UserListState {}

class UserListCubit extends Cubit<UserListState> {
  final UserRepositoryInterface _repository;

  UserListCubit(this._repository) : super(ProcessingUserListState());

  Future<void> listUsers() async {
    emit(ProcessingUserListState());

    try {
      var response = await _repository.listUsers();
      emit(SuccessUserListState(response));
    } catch (e) {
      emit(FailUserListState());
    }
  }
}
