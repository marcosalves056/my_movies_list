import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/data/exceptions/user_already_exists.dart';
import 'package:my_movies_list/data/repositories/user_repository_interface.dart';

enum RegisterAccountState { initial, processingRegister, registerFailed, userAlreadyExist, success }

class RegisterAccountCubit extends Cubit<RegisterAccountState> {
  final UserRepositoryInterface _repository;

  RegisterAccountCubit(this._repository) : super(RegisterAccountState.initial);

  Future<void> register(String email, String password, String name) async {
    emit(RegisterAccountState.processingRegister);

    try {
      await _repository.register(email, password, name);
      emit(RegisterAccountState.success);
    } on UserAlreadyExistsException {
      emit(RegisterAccountState.userAlreadyExist);
    } catch (e) {
      emit(RegisterAccountState.registerFailed);
    }
  }
}
