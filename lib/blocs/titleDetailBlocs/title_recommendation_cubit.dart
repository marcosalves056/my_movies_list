import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/data/models/title_model.dart';
import 'package:my_movies_list/data/repositories/title_repository_interface.dart';

abstract class TitleRecommendationState {}

class ProcessingTitleRecommendationState implements TitleRecommendationState {}

class SuccessTitleRecommendationState implements TitleRecommendationState {
  final List<TitleModel> recommendations;

  SuccessTitleRecommendationState(this.recommendations);

  List<TitleModel> get props => recommendations;
}

class TitleRecommendationCubit extends Cubit<TitleRecommendationState> {
  final TitleRepositoryInterface _repository;

  TitleRecommendationCubit(this._repository) : super(ProcessingTitleRecommendationState());

  Future<void> getTitleRecommendation(int titleId, {bool isTvShow = false}) async {
    emit(ProcessingTitleRecommendationState());

    var response = await _repository.getTitleRecommendation(titleId, isTvShow: isTvShow);
    emit(SuccessTitleRecommendationState(response));
  }
}
