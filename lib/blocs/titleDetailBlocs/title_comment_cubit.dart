import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/data/exceptions/comment_by_other_user.dart';
import 'package:my_movies_list/data/models/comment_model.dart';
import 'package:my_movies_list/data/repositories/title_repository_interface.dart';

abstract class TitleCommentState {}

class ProcessingTitleCommentState extends TitleCommentState {}

class SuccessTitleCommentState extends TitleCommentState {
  final List<CommentModel> comments;

  SuccessTitleCommentState(this.comments);

  List<CommentModel> get props => comments;
}

class FailTitleCommentState extends TitleCommentState {}

class CommentByOtherUserTitleCommentState extends TitleCommentState {}

class TitleCommentCubit extends Cubit<TitleCommentState> {
  final TitleRepositoryInterface _repository;

  TitleCommentCubit(this._repository) : super(ProcessingTitleCommentState());

  Future<void> getTitleComments(int titleId, {bool isTvShow = false}) async {
    emit(ProcessingTitleCommentState());

    try {
      var response = await _repository.getTitleComments(titleId, isTvShow: isTvShow);
      emit(SuccessTitleCommentState(response));
    } catch (e) {
      emit(FailTitleCommentState());
    }
  }

  Future<void> removeComment(int titleId, int commentId, {bool isTvShow = false}) async {
    try {
      var success = await _repository.removeComment(titleId, commentId, isTvShow: isTvShow);

      if (success) {
        var response = await _repository.getTitleComments(titleId, isTvShow: isTvShow);
        emit(SuccessTitleCommentState(response));
      } else {
        emit(FailTitleCommentState());
      }
    } on CommentByOtherUserException {
      emit(CommentByOtherUserTitleCommentState());
    } catch (e) {
      emit(FailTitleCommentState());
    }
  }

  Future<void> saveComment(int titleId, String comment, {bool isTvShow = false}) async {
    emit(ProcessingTitleCommentState());

    try {
      var success = await _repository.saveComment(titleId, comment, isTvShow: isTvShow);

      if (success) {
        var response = await _repository.getTitleComments(titleId, isTvShow: isTvShow);
        emit(SuccessTitleCommentState(response));
      } else {
        emit(FailTitleCommentState());
      }
    } catch (e) {
      emit(FailTitleCommentState());
    }
  }
}
