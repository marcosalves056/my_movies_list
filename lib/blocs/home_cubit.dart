// import 'dart:ffi';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/data/models/genre_model.dart';
import 'package:my_movies_list/data/models/title_model.dart';
import 'package:my_movies_list/data/repositories/title_repository_interface.dart';

enum TypeList {
  movieList,
  tvList,
  popularMovieList,
  popularTvList,
  upcomingMovie
}

class HomeState {}

class ProcessingHomeState implements HomeState {}

class SuccessHomeState implements HomeState {
  final List<TitleModel> titles;
  final String? label;

  SuccessHomeState(this.titles, {this.label});
}

class Filter implements HomeState {
  List<GenreModel>? params;
  String? label;
  TypeList typeList;

  Filter({this.params, this.label, required this.typeList});
}

class FailHomeState implements HomeState {}

class HomeCubit extends Cubit<HomeState> {
  final TitleRepositoryInterface _repository;

  HomeCubit(this._repository) : super(HomeState());

  Future<void> getTitleList(Filter filter) async {
    switch (filter.typeList) {
      case TypeList.movieList:
        for (var data in filter.params!) {
          emit(ProcessingHomeState());
          var response =
              await _repository.getMovieList(params: {'genre': data.id});
          emit(SuccessHomeState(response, label: data.name));
        }
        break;

      case TypeList.tvList:
        for (var data in filter.params!) {
          emit(ProcessingHomeState());
          var response =
              await _repository.getTvList(params: {'genre': data.id});
          emit(SuccessHomeState(response, label: data.name));
        }
        break;

      case TypeList.popularMovieList:
        emit(ProcessingHomeState());
        var response = await _repository.getPopularMovieList();
        emit(SuccessHomeState(response, label: filter.label));
        break;

      case TypeList.popularTvList:
        emit(ProcessingHomeState());
        var response = await _repository.getPopularTvList();
        emit(SuccessHomeState(response, label: filter.label));
        break;

      case TypeList.upcomingMovie:
        emit(ProcessingHomeState());
        var response = await _repository.getUpcomingMovieList();
        emit(SuccessHomeState(response, label: filter.label));
        break;

      default:
        break;
    }
  }
}
