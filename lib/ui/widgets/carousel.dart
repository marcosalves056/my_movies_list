import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Carousel extends StatelessWidget {
  final String? label;
  final List<Widget> children;

  const Carousel({this.label, required this.children, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return children.isEmpty
        ? const SizedBox()
        : Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (label != null)
                Column(
                  children: [
                    Text(
                      label!,
                      style: TextStyle(
                        color: Theme.of(context).primaryColor,
                        fontSize: 16.0,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    const SizedBox(height: 10.0),
                  ],
                ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(children: children),
              )
            ],
          );
  }
}
