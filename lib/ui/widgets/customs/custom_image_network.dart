import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CustomImageNetWork extends StatelessWidget {
  final String urlImage;
  final double? height;
  final String? imageErrorText;

  const CustomImageNetWork({
    Key? key,
    required this.urlImage,
    this.height,
    this.imageErrorText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image.network(
      urlImage,
      height: height,
      loadingBuilder: (context, child, loadingProgress) {
        if (loadingProgress == null) {
          return child;
        }

        return Container(
          color: Theme.of(context).primaryColor.withOpacity(0.1),
          height: height,
          child: const Center(
            child: CircularProgressIndicator(
              color: Colors.white,
            ),
          ),
        );
      },
    );
  }
}
