import 'package:flutter/material.dart';

class CustomTextFormField extends StatelessWidget {
  final String labelText;
  final bool obscureText;
  final TextInputType? keyboardType;
  final Widget? icon;
  final TextEditingController? controller;
  final Widget? suffix;

  const CustomTextFormField({
    Key? key,
    required this.labelText,
    this.obscureText = false,
    this.keyboardType,
    this.icon,
    this.controller,
    this.suffix,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      decoration: InputDecoration(
        border: const OutlineInputBorder(),
        labelText: labelText,
        icon: icon,
        suffixIcon: suffix,
      ),
      obscureText: obscureText,
      keyboardType: keyboardType,
    );
  }
}
