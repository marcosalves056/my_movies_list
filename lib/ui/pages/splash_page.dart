import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/blocs/splash_cubit.dart';
import 'package:my_movies_list/data/repositories/user_repository_interface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/pages/home_page.dart';
import 'package:my_movies_list/ui/pages/login_page.dart';

class SplashPage extends StatelessWidget {
  static const name = 'splash-page';

  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => SplashCubit(getIt.get<UserRepositoryInterface>())..checkUser(),
      child: const SplashView(),
    );
  }
}

class SplashView extends StatelessWidget {
  const SplashView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<SplashCubit, SplashState>(
      listener: (BuildContext context, state) {
        if (state == SplashState.userLogged) {
          Navigator.pushReplacementNamed(context, HomePage.name);
        } else if (state == SplashState.userNotLogged) {
          Navigator.pushReplacementNamed(context, LoginPage.name);
        }
      },
      child: Scaffold(
        body: Center(
          child: Icon(
            Icons.movie,
            size: 100.0,
            color: Theme.of(context).primaryColor,
          ),
        ),
      ),
    );
  }
}
