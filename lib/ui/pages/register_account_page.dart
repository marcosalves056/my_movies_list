import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:my_movies_list/blocs/register_account_cubit.dart';
import 'package:my_movies_list/data/repositories/user_repository_interface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/pages/home_page.dart';
import 'package:my_movies_list/ui/widgets/customs/custom_loading_elevated_button.dart';
import 'package:my_movies_list/ui/widgets/customs/custom_text_form_field.dart';

class RegisterAccountPage extends StatelessWidget {
  static const name = 'register-account-page';

  const RegisterAccountPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => RegisterAccountCubit(getIt.get<UserRepositoryInterface>()),
      child: RegisterAccountView(),
    );
  }
}

class RegisterAccountView extends StatelessWidget {
  RegisterAccountView({Key? key}) : super(key: key);

  final nameController = TextEditingController();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BlocListener<RegisterAccountCubit, RegisterAccountState>(
      listener: (context, state) {
        if (state == RegisterAccountState.success) {
          Navigator.pushNamedAndRemoveUntil(context, HomePage.name, (route) => false);
        }
      },
      child: SafeArea(
        child: Scaffold(
          body: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            child: Center(
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Center(
                      child: FaIcon(
                        FontAwesomeIcons.userPlus,
                        size: 45,
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                    const SizedBox(height: 15.0),
                    Text(
                      'Crie sua conta',
                      style: TextStyle(
                        color: Theme.of(context).primaryColor,
                        fontSize: 20.0,
                        fontWeight: FontWeight.w300,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    const SizedBox(height: 25.0),
                    CustomTextFormField(
                      labelText: 'Nome',
                      controller: nameController,
                    ),
                    const SizedBox(height: 15.0),
                    CustomTextFormField(
                      labelText: 'Email',
                      controller: emailController,
                      keyboardType: TextInputType.emailAddress,
                    ),
                    const SizedBox(height: 15.0),
                    CustomTextFormField(
                      labelText: 'Senha',
                      controller: passwordController,
                      obscureText: true,
                    ),
                    const SizedBox(height: 25.0),
                    BlocBuilder<RegisterAccountCubit, RegisterAccountState>(
                      builder: (context, state) {
                        return Visibility(
                          child: Text(
                            state == RegisterAccountState.userAlreadyExist ? 'Usuário já existe' : 'Falha ao cadastrar usuário',
                            style: const TextStyle(color: Colors.red),
                          ),
                          visible: state == RegisterAccountState.registerFailed || state == RegisterAccountState.userAlreadyExist,
                        );
                      },
                    ),
                    BlocBuilder<RegisterAccountCubit, RegisterAccountState>(
                      builder: (context, state) {
                        return CustomLoadingElevatedButton(
                          child: const Text('Entrar'),
                          isLoading: state == RegisterAccountState.processingRegister,
                          onPressed: () => context.read<RegisterAccountCubit>().register(
                                emailController.text,
                                passwordController.text,
                                nameController.text,
                              ),
                        );
                      },
                    ),
                    TextButton(
                      child: const Text('Voltar'),
                      onPressed: () => Navigator.pop(context),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
