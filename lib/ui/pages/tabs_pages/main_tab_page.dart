import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/blocs/home_cubit.dart';
import 'package:my_movies_list/ui/widgets/title_carousel.dart';

class MainTabPage extends StatelessWidget {
  const MainTabPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: [
                _buildUpcomingMovieList(context, 'Filmes Recentes'),
                _buildPopularMovieList(context, 'Filmes Populares'),
                _buildPopularTvList(context, 'Series Populares'),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildList(String label) {
    return BlocBuilder<HomeCubit, HomeState>(
      buildWhen: (previous, current) {
        if (current is SuccessHomeState) {
          return current.label == label;
        }

        return true;
      },
      builder: (context, state) {
        if (state is ProcessingHomeState) {
          return const Center(child: CircularProgressIndicator());
        }

        return Padding(
          padding: const EdgeInsets.symmetric(vertical: 5.0),
          child: TitleCarousel(
            label: label,
            list: (state as SuccessHomeState).titles,
          ),
        );
      },
    );
  }

  Widget _buildUpcomingMovieList(BuildContext context, String label) {
    context.read<HomeCubit>().getTitleList(
          Filter(typeList: TypeList.upcomingMovie, label: label),
        );
    return _buildList(label);
  }

  Widget _buildPopularMovieList(BuildContext context, String label) {
    context.read<HomeCubit>().getTitleList(
          Filter(typeList: TypeList.popularMovieList, label: label),
        );
    return _buildList(label);
  }

  Widget _buildPopularTvList(BuildContext context, String label) {
    context.read<HomeCubit>().getTitleList(
          Filter(typeList: TypeList.popularTvList, label: label),
        );
    return _buildList(label);
  }
}
