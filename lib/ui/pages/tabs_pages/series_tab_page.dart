import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/blocs/home_cubit.dart';
import 'package:my_movies_list/data/models/genre_model.dart';
import 'package:my_movies_list/ui/widgets/title_carousel.dart';

class SeriesTabPage extends StatelessWidget {
  SeriesTabPage({Key? key}) : super(key: key);

  final genres = <GenreModel>[
    GenreModel(id: 16, name: 'Animação'),
    GenreModel(id: 12, name: 'Aventura'),
    GenreModel(id: 18, name: 'Drama'),
    GenreModel(id: 99, name: 'Documentário'),
  ];

  @override
  Widget build(BuildContext context) {
    context.read<HomeCubit>().getTitleList(
          Filter(typeList: TypeList.tvList, params: genres),
        );
    return SingleChildScrollView(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: genres.map((e) {
                return BlocBuilder<HomeCubit, HomeState>(
                  buildWhen: (previous, current) {
                    if (current is SuccessHomeState) {
                      return current.label == e.name;
                    }

                    return true;
                  },
                  builder: (context, state) {
                    if (state is ProcessingHomeState) {
                      return const Center(child: CircularProgressIndicator());
                    }

                    return Padding(
                      padding: const EdgeInsets.symmetric(vertical: 5.0),
                      child: TitleCarousel(
                        label: e.name,
                        list: (state as SuccessHomeState).titles,
                      ),
                    );
                  },
                );
              }).toList(),
            ),
          ),
        ],
      ),
    );
  }
}
