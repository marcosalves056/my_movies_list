import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

import 'package:my_movies_list/blocs/titleDetailBlocs/title_comment_cubit.dart';
import 'package:my_movies_list/blocs/titleDetailBlocs/title_detail_cubit.dart';
import 'package:my_movies_list/blocs/title_rate_cubit.dart';
import 'package:my_movies_list/blocs/titleDetailBlocs/title_recommendation_cubit.dart';
import 'package:my_movies_list/data/models/title_detail_model.dart';

import 'package:my_movies_list/data/repositories/title_repository_interface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/widgets/carousel.dart';
import 'package:my_movies_list/ui/widgets/customs/custom_text_form_field.dart';
import 'package:my_movies_list/ui/widgets/title_rate.dart';
import 'package:my_movies_list/ui/widgets/title_thumbnail.dart';

class TitleDetailsPage extends StatelessWidget {
  static const name = 'title-details-page';

  const TitleDetailsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final arguments = (ModalRoute.of(context)!.settings.arguments as Map);
    final titleId = arguments['id'] as int;
    final isTvShow = arguments['is_tv_show'] as bool;

    return MultiBlocProvider(
      providers: [
        BlocProvider<TitleDetailCubit>(
          create: (contex) =>
              TitleDetailCubit(getIt.get<TitleRepositoryInterface>())
                ..getTitleDetails(titleId, isTvShow: isTvShow),
        ),
        BlocProvider<TitleRateCubit>(
          create: (contex) =>
              TitleRateCubit(getIt.get<TitleRepositoryInterface>())
                ..getTitleRate(titleId, isTvShow: isTvShow),
        ),
        BlocProvider<TitleRecommendationCubit>(
          create: (contex) =>
              TitleRecommendationCubit(getIt.get<TitleRepositoryInterface>())
                ..getTitleRecommendation(titleId, isTvShow: isTvShow),
        ),
        BlocProvider<TitleCommentCubit>(
          create: (contex) =>
              TitleCommentCubit(getIt.get<TitleRepositoryInterface>())
                ..getTitleComments(titleId, isTvShow: isTvShow),
        ),
      ],
      child: TitleDetailsView(isTvShow: isTvShow, titleId: titleId),
    );
  }
}

class TitleDetailsView extends StatelessWidget {
  final int titleId;
  final bool isTvShow;

  TitleDetailsView({Key? key, required this.titleId, required this.isTvShow})
      : super(key: key);

  final _commentController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final arguments = (ModalRoute.of(context)!.settings.arguments as Map);
    final isTvShow = arguments['is_tv_show'] as bool;

    return SafeArea(
      child: BlocBuilder<TitleDetailCubit, TitleDetailState>(
        builder: (context, state) {
          if (state is ProcessingTitleDetailState) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }

          if (state is FailTitleDetailState) {
            return const Center(
              child: Text('Falha ao carregar os detalhes do título'),
            );
          }

          var titleDetail = (state as SuccessTitleDetailState).detail;

          return Scaffold(
            appBar: AppBar(
              title: Text(titleDetail.name),
              backgroundColor: Colors.black,
              actions: const [],
            ),
            body: Column(
              children: [
                Image.network(titleDetail.coverUrl),
                Expanded(
                  child: _buildDetails(
                      context: context,
                      titleDetail: titleDetail,
                      isTvShow: isTvShow),
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  Widget _buildDetails(
      {required BuildContext context,
      required TitleDetailModel titleDetail,
      required bool isTvShow}) {
    return Container(
      margin: const EdgeInsets.fromLTRB(12.0, 12.0, 12.0, 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  if (titleDetail.releaseDate != null)
                    Text(
                      '(${DateFormat('dd/MM/yyyy').format(titleDetail.releaseDate!)})',
                      style: const TextStyle(fontWeight: FontWeight.w700),
                    ),
                  const SizedBox(height: 10.0),
                  const Text(
                    'Sinopse:',
                    style: TextStyle(fontWeight: FontWeight.w700),
                  ),
                  const SizedBox(height: 5.0),
                  Text(
                    titleDetail.overview,
                    style: const TextStyle(fontSize: 15.0),
                  ),
                  if (titleDetail.runtime.isEmpty || titleDetail.runtime != '0')
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: Text('Duração: ${titleDetail.runtime} min'),
                    ),
                  const SizedBox(height: 10.0),
                  _buildCarouselGenre(genres: titleDetail.genres),
                  const SizedBox(height: 5.0),
                  _buildRate(),
                  _buildRecommendations(),
                  _buildComments()
                ],
              ),
            ),
          ),
          const SizedBox(height: 10.0),
          BlocBuilder<TitleCommentCubit, TitleCommentState>(
              builder: (context, state) {
            return CustomTextFormField(
              labelText: 'Adicione um comentário',
              controller: _commentController,
              suffix: IconButton(
                icon: const Icon(Icons.send_rounded),
                onPressed: () async {
                  context.read<TitleCommentCubit>().saveComment(
                      titleDetail.id, _commentController.text,
                      isTvShow: isTvShow);
                  _commentController.clear();
                },
              ),
            );
          }),
        ],
      ),
    );
  }

  Widget _buildCarouselGenre({required List<String> genres}) {
    return Carousel(
      children: genres
          .map(
            (String e) => Padding(
              padding: const EdgeInsets.symmetric(horizontal: 2.5),
              child: Chip(
                label: Text(e),
              ),
            ),
          )
          .toList(),
    );
  }

  Widget _buildRate() {
    return BlocBuilder<TitleRateCubit, TitleRateState>(
      builder: (context, state) {
        if (state is ProcessingTitleRateState) {
          return const Center(child: CircularProgressIndicator());
        }

        return TitleRate(
          value: state is SuccessTitleRateState ? (state.rate) : null,
          onChanged: (bool value) {
            context
                .read<TitleRateCubit>()
                .saveTitleRate(titleId, value ? 1 : -1, isTvShow: isTvShow);
          },
        );
      },
    );
  }

  Widget _buildRecommendations() {
    return BlocBuilder<TitleRecommendationCubit, TitleRecommendationState>(
      builder: (context, state) {
        if (state is ProcessingTitleRecommendationState) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }

        if (state is SuccessTitleRecommendationState) {
          var recommendations = state.recommendations;
          return Padding(
            padding: const EdgeInsets.only(bottom: 10.0),
            child: Carousel(
              label: 'Recomendados',
              children: recommendations
                  .map(
                    (e) => GestureDetector(
                      onTap: () => Navigator.pushNamed(
                          context, TitleDetailsPage.name,
                          arguments: {'id': titleId, 'is_tv_show': isTvShow}),
                      child: TitleThumbnail(
                        width: 100.0,
                        height: 150.0,
                        titleName: e.name,
                        showBanner: false,
                        urlThumbnail: e.posterUrl,
                      ),
                    ),
                  )
                  .toList(),
            ),
          );
        } else {
          return const SizedBox();
        }
      },
    );
  }

  Widget _buildComments() {
    return BlocListener<TitleCommentCubit, TitleCommentState>(
      listener: (context, state) {
        if (state is CommentByOtherUserTitleCommentState) {
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
              content: Text(
                  'Não é permitido excluir um comentário feito por outro usuário')));
        }

        if (state is FailTitleCommentState) {
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
              content: Text(
                  'Não é permitido excluir um comentário feito por outro usuário')));
        }
      },
      child: BlocBuilder<TitleCommentCubit, TitleCommentState>(
        buildWhen: (previous, current) {
          return (current is ProcessingTitleCommentState ||
              current is SuccessTitleCommentState);
        },
        builder: (context, state) {
          if (state is ProcessingTitleCommentState) {
            return const Center(child: CircularProgressIndicator());
          }

          if (state is SuccessTitleCommentState) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const Text(
                  'Comentários',
                  style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                ...state.comments.reversed
                    .take(5)
                    .map(
                      (e) => e.text.isNotEmpty
                          ? Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(
                                  child: ListTile(
                                    title: Text(
                                      e.text,
                                      maxLines: 1,
                                      overflow: TextOverflow.clip,
                                    ),
                                    subtitle: Text(
                                      DateFormat('dd/MM/yyyy hh:mm:ss')
                                          .format(e.date),
                                    ),
                                  ),
                                ),
                                IconButton(
                                  onPressed: () => context
                                      .read<TitleCommentCubit>()
                                      .removeComment(titleId, e.id,
                                          isTvShow: isTvShow),
                                  icon:
                                      const Icon(Icons.delete_outline_outlined),
                                ),
                              ],
                            )
                          : const SizedBox(),
                    )
                    .toList(),
                if (state.comments.length > 5)
                  TextButton(
                    onPressed: () {},
                    child: Text(
                      'Ver todos os ${state.comments.length} comentários',
                      textAlign: TextAlign.start,
                    ),
                  ),
              ],
            );
          } else {
            return const SizedBox();
          }
        },
      ),
    );
  }
}
