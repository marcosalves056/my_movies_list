import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/blocs/rated_title_list_cubit.dart';
import 'package:my_movies_list/blocs/title_rate_cubit.dart';
import 'package:my_movies_list/data/models/title_rated_model.dart';
import 'package:my_movies_list/data/repositories/title_repository_interface.dart';
import 'package:my_movies_list/locator.dart';

import 'package:my_movies_list/ui/widgets/customs/custom_image_network.dart';
import 'package:my_movies_list/ui/widgets/title_rate.dart';

class RatedTitleListPage extends StatelessWidget {
  static const name = 'rated-title-page';

  const RatedTitleListPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final args = (ModalRoute.of(context)!.settings.arguments as Map? ?? {});
    final String? userId = args['user_id'];
    final String? userName = args['user_name'];

    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => RatedTitleListCubit(getIt.get<TitleRepositoryInterface>())..getUserRatedTitleList(userId)),
        BlocProvider(create: (context) => TitleRateCubit(getIt.get<TitleRepositoryInterface>())),
      ],
      child: RatedTitleListView(userId: userId, userName: userName),
    );
  }
}

class RatedTitleListView extends StatelessWidget {
  final String? userId;
  final String? userName;

  const RatedTitleListView({Key? key, this.userId, this.userName}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text(userName ?? 'Minhas avaliações'),
        ),
        body: Padding(
          padding: const EdgeInsets.all(10),
          child: SingleChildScrollView(
            child: BlocBuilder<RatedTitleListCubit, RatedTitleListState>(
              builder: (context, state) {
                if (state is ProcessingRatedTitleListState) {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                }

                if (state is FailRatedTitleListState) {
                  return const Center(
                    child: Text('Falha ao carregar os títulos avaliados'),
                  );
                }

                if (state is SuccessRatedTitleListState) {
                  return Column(
                    children: state.ratings.map((e) => _buildTitleCard(context: context, title: e, canRate: userId == null)).toList(),
                  );
                } else {
                  return const SizedBox();
                }
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildTitleCard({required TitleRatedModel title, required BuildContext context, bool canRate = false}) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  height: 90.0,
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(2.5),
                    child: CustomImageNetWork(
                      urlImage: title.posterUrl,
                      height: 80.0,
                    ),
                  ),
                ),
                const SizedBox(width: 10.0),
                Expanded(
                  child: Text(
                    title.name,
                    style: const TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ),
          BlocBuilder<TitleRateCubit, TitleRateState>(
            buildWhen: (previous, current) {
              if (current is SuccessTitleRateState) {
                return current.titleId == title.id;
              }

              return true;
            },
            builder: (context, state) {
              bool? _rate = title.rate != null ? (title.rate == 1) : null;

              if (state is SuccessTitleRateState) {
                _rate = state.rate;
              }

              return TitleRate(
                value: _rate,
                onChanged: canRate ? (bool value) => context.read<TitleRateCubit>().saveTitleRate(title.id, value ? 1 : -1) : null,
              );
            },
          ),
        ],
      ),
    );
  }
}
