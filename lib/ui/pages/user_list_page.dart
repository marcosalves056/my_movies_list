import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/blocs/user_list_cubit.dart';
import 'package:my_movies_list/data/models/user_model.dart';
import 'package:my_movies_list/data/repositories/user_repository_interface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/pages/rated_title_list_page.dart';

class UserListPage extends StatelessWidget {
  static const name = 'user-list-page';

  const UserListPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => UserListCubit(getIt.get<UserRepositoryInterface>())..listUsers(),
      child: const UserListView(),
    );
  }
}

class UserListView extends StatelessWidget {
  const UserListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Lista de usuários'),
        ),
        body: Padding(
          padding: const EdgeInsets.all(10.0),
          child: SingleChildScrollView(
            child: BlocBuilder<UserListCubit, UserListState>(
              builder: (context, state) {
                if (state is ProcessingUserListState) {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                }

                if (state is FailUserListState) {
                  return const Center(
                    child: Text('Falha ao carregar os usuários'),
                  );
                }

                if (state is SuccessUserListState) {
                  return Column(
                    children: state.users.map((e) => _buildUsers(context: context, user: e)).toList(),
                  );
                } else {
                  return const SizedBox();
                }
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildUsers({required UserModel user, required BuildContext context}) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5.0),
      child: GestureDetector(
        onTap: () => Navigator.pushNamed(context, RatedTitleListPage.name, arguments: {'user_id': user.id, 'user_name': user.name}),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            CircleAvatar(
              child: Text(
                user.name[0].toUpperCase(),
                style: const TextStyle(
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            const SizedBox(width: 10.0),
            Expanded(
              child: Text(
                user.name,
                style: const TextStyle(fontSize: 16.0),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
