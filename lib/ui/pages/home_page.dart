import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:my_movies_list/blocs/home_cubit.dart';
import 'package:my_movies_list/data/repositories/title_repository_interface.dart';
import 'package:my_movies_list/data/repositories/user_repository_interface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/pages/login_page.dart';
import 'package:my_movies_list/ui/pages/rated_title_list_page.dart';
import 'package:my_movies_list/ui/pages/search_page.dart';
import 'package:my_movies_list/ui/pages/tabs_pages/main_tab_page.dart';
import 'package:my_movies_list/ui/pages/tabs_pages/movies_tab_page.dart';
import 'package:my_movies_list/ui/pages/tabs_pages/series_tab_page.dart';
import 'package:my_movies_list/ui/pages/user_list_page.dart';

class HomePage extends StatelessWidget {
  static const name = 'home-page';

  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => HomeCubit(getIt.get<TitleRepositoryInterface>()),
      child: const HomeView(),
    );
  }
}

class HomeView extends StatelessWidget {
  static const name = 'home-page';

  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            title: const Text(
              'Catálogo de filmes e séries',
              style: TextStyle(fontSize: 18),
            ),
            actions: [
              IconButton(
                icon: const FaIcon(FontAwesomeIcons.users, size: 15),
                onPressed: () =>
                    Navigator.pushNamed(context, UserListPage.name),
              ),
              IconButton(
                icon: const FaIcon(FontAwesomeIcons.history, size: 15),
                onPressed: () =>
                    Navigator.pushNamed(context, RatedTitleListPage.name),
              ),
              IconButton(
                icon: const FaIcon(FontAwesomeIcons.search, size: 15),
                onPressed: () => Navigator.pushNamed(context, SearchPage.name),
              ),
              IconButton(
                  onPressed: () async {
                    await getIt.get<UserRepositoryInterface>().clearSession();
                    Navigator.pushNamedAndRemoveUntil(
                        context, LoginPage.name, (route) => false);
                  },
                  icon: const Icon(Icons.exit_to_app)),
            ],
          ),
          bottomNavigationBar: TabBar(
            labelColor: Colors.blue,
            indicatorSize: TabBarIndicatorSize.label,
            indicatorColor: Colors.blue,
            indicatorPadding: const EdgeInsets.all(5.0),
            unselectedLabelColor: Colors.blue.withOpacity(0.5),
            tabs: const [
              Tab(icon: FaIcon(FontAwesomeIcons.home)),
              Tab(icon: FaIcon(FontAwesomeIcons.film)),
              Tab(icon: FaIcon(FontAwesomeIcons.tv)),
            ],
          ),
          body: TabBarView(
            physics: const NeverScrollableScrollPhysics(),
            children: [
              const MainTabPage(),
              MoviesTabPage(),
              SeriesTabPage(),
            ],
          ),
        ),
      ),
    );
  }
}
