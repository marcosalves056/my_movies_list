import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/blocs/login_cubit.dart';
import 'package:my_movies_list/data/repositories/user_repository_interface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/pages/home_page.dart';
import 'package:my_movies_list/ui/pages/register_account_page.dart';
import 'package:my_movies_list/ui/widgets/customs/custom_loading_elevated_button.dart';
import 'package:my_movies_list/ui/widgets/customs/custom_text_form_field.dart';

class LoginPage extends StatelessWidget {
  static const name = 'login-page';

  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => LoginCubit(getIt.get<UserRepositoryInterface>()),
      child: LoginView(),
    );
  }
}

class LoginView extends StatelessWidget {
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  LoginView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginCubit, LoginState>(
      listener: (context, state) {
        if (state == LoginState.success) {
          Navigator.pushReplacementNamed(context, HomePage.name);
        }
      },
      child: SafeArea(
        child: Scaffold(
          body: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Center(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text(
                      'Informe suas credenciais para começar',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Theme.of(context).primaryColor,
                        fontSize: 20.0,
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                    const SizedBox(height: 25.0),
                    CustomTextFormField(
                      labelText: 'Email',
                      controller: emailController,
                      keyboardType: TextInputType.emailAddress,
                    ),
                    const SizedBox(height: 20.0),
                    CustomTextFormField(
                      labelText: 'Senha',
                      controller: passwordController,
                      obscureText: true,
                    ),
                    const SizedBox(height: 25.0),
                    BlocBuilder<LoginCubit, LoginState>(
                      builder: (context, state) {
                        return Visibility(
                          visible: state == LoginState.loginFailed || state == LoginState.userNotFound,
                          child: Text(
                            state == LoginState.userNotFound ? 'Usuário não encontrado' : 'Falha ao realizar login',
                            style: const TextStyle(color: Colors.red),
                          ),
                        );
                      },
                    ),
                    BlocBuilder<LoginCubit, LoginState>(
                      builder: (context, state) {
                        return CustomLoadingElevatedButton(
                          child: const Text('Entrar'),
                          isLoading: state == LoginState.processingLogin,
                          onPressed: () {
                            context.read<LoginCubit>().login(emailController.text, passwordController.text);
                          },
                        );
                      },
                    ),
                    const SizedBox(height: 15.0),
                    Text(
                      'OU',
                      style: TextStyle(
                        color: Theme.of(context).primaryColor,
                        fontSize: 12.0,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    TextButton(
                      child: const Text('Criar minha conta'),
                      onPressed: () => Navigator.pushNamed(context, RegisterAccountPage.name),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
