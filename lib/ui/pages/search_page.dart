import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/blocs/search_cubit.dart';
import 'package:my_movies_list/data/models/title_model.dart';
import 'package:my_movies_list/data/repositories/title_repository_interface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/pages/title_details_page.dart';
import 'package:my_movies_list/ui/widgets/title_thumbnail.dart';

class SearchPage extends StatelessWidget {
  static const name = 'search-page';

  const SearchPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => SearchCubit(getIt.get<TitleRepositoryInterface>()),
      child: SearchView(),
    );
  }
}

class SearchView extends StatelessWidget {
  final searchController = TextEditingController();
  final scrollController = ScrollController();
  final _scrollThreshold = 200.0;

  SearchView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    void _onScroll() {
      final maxScroll = scrollController.position.maxScrollExtent;
      final currentScroll = scrollController.position.pixels;
      if (maxScroll - currentScroll <= _scrollThreshold) {
        context.read<SearchCubit>().getMovieList(searchController.text);
      }
    }

    scrollController.addListener(_onScroll);

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: TextFormField(
            style: const TextStyle(color: Colors.white),
            controller: searchController,
            decoration: const InputDecoration(
              border: InputBorder.none,
              hintText: 'Pesquise aqui...',
              hintStyle: TextStyle(color: Colors.white30),
            ),
            onFieldSubmitted: (value) => context.read<SearchCubit>().getMovieList(searchController.text),
          ),
        ),
        body: BlocBuilder<SearchCubit, SearchState>(
          builder: (context, state) {
            if (state is ProcessingSearchState) {
              return const Center(child: CircularProgressIndicator());
            }

            if (state is SuccessSearchState) {
              return GridView.count(
                controller: scrollController,
                padding: const EdgeInsets.all(5.0),
                children: state.titles.map((e) => _buildTitleCard(context, e)).toList(),
                crossAxisCount: 2,
                mainAxisSpacing: 5.0,
              );
            } else {
              return const SizedBox();
            }
          },
        ),
      ),
    );
  }

  Widget _buildTitleCard(BuildContext context, TitleModel title) {
    return GestureDetector(
      onTap: () => Navigator.pushNamed(context, TitleDetailsPage.name, arguments: {'id': title.id, 'is_tv_show': title.isTvShow}),
      child: TitleThumbnail(
        width: 100.0,
        height: 1200.0,
        titleName: title.name,
        urlThumbnail: title.posterUrl,
      ),
    );
  }
}
