import 'package:my_movies_list/data/models/comment_model.dart';

class TitleDetailModel {
  int id;
  String name;
  String overview;
  DateTime? releaseDate;
  num voteAverage;
  String homepage;
  String runtime;
  String coverUrl;
  String posterUrl;
  List<String> genres;
  List<CommentModel> comments;

  TitleDetailModel({
    required this.id,
    required this.name,
    required this.overview,
    required this.releaseDate,
    required this.voteAverage,
    required this.homepage,
    required this.runtime,
    required this.coverUrl,
    required this.posterUrl,
    required this.genres,
    required this.comments,
  });

  factory TitleDetailModel.fromJson(Map<String, dynamic> json) {
    return TitleDetailModel(
      id: json['id'],
      name: json['name'],
      overview: json['overview'],
      releaseDate: json['release_date'] != null && (json['release_date'] as String).isNotEmpty ? DateTime.parse(json['release_date']) : null,
      voteAverage: json['vote_average'],
      homepage: json['homepage'],
      runtime: json['runtime'],
      coverUrl: json['cover_url'],
      posterUrl: json['poster_url'],
      genres: List<String>.from(json['genres'].map((e) => e['name'])).toList(),
      comments: List<CommentModel>.from(json['comments'].map((e) => CommentModel.fromJson(e))).toList(),
    );
  }
}
