import 'package:my_movies_list/data/models/comment_model.dart';
import 'package:my_movies_list/data/models/title_detail_model.dart';
import 'package:my_movies_list/data/models/title_model.dart';
import 'package:my_movies_list/data/models/title_rated_model.dart';

abstract class TitleRepositoryInterface {
  Future<List<TitleModel>> getMovieList({Map<String, dynamic>? params});

  Future<int> getCountMovieList({Map<String, dynamic>? params});

  Future<List<TitleModel>> getPopularMovieList({Map<String, dynamic>? params});

  Future<List<TitleModel>> getUpcomingMovieList({Map<String, dynamic>? params});

  Future<List<TitleModel>> getTvList({Map<String, dynamic>? params});

  Future<List<TitleModel>> getPopularTvList({Map<String, dynamic>? params});

  Future<List<TitleModel>> getTitleRecommendation(int titleId, {bool isTvShow = false});

  Future<TitleDetailModel> getTitleDetails(int titleId, {bool isTvShow = false});

  Future<List<CommentModel>> getTitleComments(int titleId, {bool isTvShow = false});

  Future<bool> removeComment(int titleId, int commentId, {bool isTvShow = false});

  Future<bool> saveComment(int titleId, String comment, {bool isTvShow = false});

  Future<int> getTitleRate(int titleId, {bool isTvShow = false});

  Future<bool> saveTitleRate(int titleId, int rate, {bool isTvShow = false});

  Future<List<TitleRatedModel>> getUserRatedTitleList(String? userId);
}
