import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:my_movies_list/data/exceptions/user_already_exists.dart';
import 'package:my_movies_list/data/exceptions/user_not_found.dart';
import 'package:my_movies_list/data/models/user_model.dart';
import 'package:my_movies_list/data/repositories/user_repository_interface.dart';
import 'package:my_movies_list/data/services/http_service.dart';
import 'package:my_movies_list/shared/strings.dart';

class UserRepository implements UserRepositoryInterface {
  final HttpService _service;
  final String _baseUrl = Strings.movieApiUrl;
  final FlutterSecureStorage _storage = const FlutterSecureStorage();
  UserModel? user;

  UserRepository(this._service);

  @override
  Future<String?> getToken() async {
    return await _storage.read(key: 'auth_token');
  }

  @override
  Future<UserModel> getUser() async {
    if (user == null) {
      final uri = '$_baseUrl/auth/me';
      final token = await getToken();
      final headers = {'Authorization': 'Bearer $token'};
      final response = await _service.getRequest(uri, headers: headers);

      user = UserModel.fromJson(response.content!);
    }

    return user!;
  }

  @override
  Future<UserModel> login(String email, String password) async {
    final uri = '$_baseUrl/auth/login';
    final body = {'email': email, 'password': password};
    final response = await _service.postRequest(uri, body);

    if (response.sucess) {
      var token = response.content!['token'];

      await saveToken(token);
      return getUser();
    }

    if (response.statusCode == 400) {
      throw UserNotFoundException();
    }

    throw Exception('Falha ao realizar login');
  }

  @override
  Future<UserModel> register(String email, String password, String name) async {
    final uri = '$_baseUrl/auth/register';
    final body = {'email': email, 'password': password, 'name': name};
    final response = await _service.postRequest(uri, body);

    if (response.sucess) {
      var token = response.content!['token'];

      await saveToken(token);
      return getUser();
    }

    if (response.statusCode == 400) {
      throw UserAlreadyExistsException();
    }

    throw Exception('Falha ao cadastrar o usuário');
  }

  @override
  Future<void> saveToken(String token) async {
    await _storage.write(key: 'auth_token', value: token);
  }

  @override
  Future<bool> isLogged() async {
    final token = await getToken();
    return token != null;
  }

  @override
  Future<void> clearSession() async {
    _storage.delete(key: 'auth_token');
  }

  @override
  Future<List<UserModel>> listUsers() async {
    final uri = '$_baseUrl/users';
    final token = await getToken();
    final headers = {'Authorization': 'Bearer $token'};
    final response = await _service.getRequest(uri, headers: headers);

    if (response.sucess) {
      List<dynamic> data = response.content!['data'];
      return List<UserModel>.from(data.map((json) => UserModel.fromJson(json)));
    }

    return [];
  }
}
