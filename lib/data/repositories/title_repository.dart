import 'package:my_movies_list/data/exceptions/comment_by_other_user.dart';
import 'package:my_movies_list/data/exceptions/title_not_rated.dart';
import 'package:my_movies_list/data/models/comment_model.dart';
import 'package:my_movies_list/data/models/title_detail_model.dart';
import 'package:my_movies_list/data/models/title_model.dart';
import 'package:my_movies_list/data/models/title_rated_model.dart';
import 'package:my_movies_list/data/repositories/title_repository_interface.dart';
import 'package:my_movies_list/data/repositories/user_repository_interface.dart';
import 'package:my_movies_list/data/services/http_service.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/shared/strings.dart';

class TitleRepository implements TitleRepositoryInterface {
  final HttpService _service;
  final String _baseUrl = Strings.movieApiUrl;

  TitleRepository(this._service);

  @override
  Future<List<TitleModel>> getMovieList({Map<String, dynamic>? params}) async {
    final uri = '$_baseUrl/movies/';
    var response = await _service.getRequest(uri, params: params);

    if (response.sucess) {
      List<dynamic> data = response.content!['data'];
      return List<TitleModel>.from(data.map((json) => TitleModel.fromJson(json)));
    }

    return [];
  }

  @override
  Future<int> getCountMovieList({Map<String, dynamic>? params}) async {
    final uri = '$_baseUrl/movies/';
    var response = await _service.getRequest(uri, params: params);

    if (response.sucess) {
      return response.content!['count'];
    }

    return 0;
  }

  @override
  Future<List<TitleModel>> getPopularMovieList({Map<String, dynamic>? params}) async {
    final uri = '$_baseUrl/movies/popular';
    var response = await _service.getRequest(uri, params: params);

    if (response.sucess) {
      List<dynamic> data = response.content!['data'];
      return List<TitleModel>.from(data.map((json) => TitleModel.fromJson(json)));
    }

    return [];
  }

  @override
  Future<List<TitleModel>> getUpcomingMovieList({Map<String, dynamic>? params}) async {
    final uri = '$_baseUrl/movies/upcoming';
    var response = await _service.getRequest(uri, params: params);

    if (response.sucess) {
      List<dynamic> data = response.content!['data'];
      return List<TitleModel>.from(data.map((json) => TitleModel.fromJson(json)));
    }

    return [];
  }

  @override
  Future<List<TitleModel>> getTvList({Map<String, dynamic>? params}) async {
    final uri = '$_baseUrl/tv/';
    var response = await _service.getRequest(uri, params: params);

    if (response.sucess) {
      List<dynamic> data = response.content!['data'];
      return List<TitleModel>.from(data.map((json) => TitleModel.fromJson(json)));
    }

    return [];
  }

  @override
  Future<List<TitleModel>> getPopularTvList({Map<String, dynamic>? params}) async {
    final uri = '$_baseUrl/tv/popular';
    var response = await _service.getRequest(uri, params: params);

    if (response.sucess) {
      List<dynamic> data = response.content!['data'];
      return List<TitleModel>.from(data.map((json) => TitleModel.fromJson(json)));
    }

    return [];
  }

  @override
  Future<List<TitleModel>> getTitleRecommendation(int titleId, {bool isTvShow = false}) async {
    final uri = '$_baseUrl/${isTvShow ? 'tv' : 'movies'}/$titleId/recommendations';
    var response = await _service.getRequest(uri);

    if (response.sucess) {
      List<dynamic> data = response.content!['data'];
      return List<TitleModel>.from(data.map((json) => TitleModel.fromJson(json)));
    }

    return [];
  }

  @override
  Future<TitleDetailModel> getTitleDetails(int titleId, {bool isTvShow = false}) async {
    final uri = '$_baseUrl/${isTvShow ? 'tv' : 'movies'}/$titleId';
    var response = await _service.getRequest(uri);

    if (response.sucess) {
      return TitleDetailModel.fromJson(response.content!);
    } else {
      throw Exception(response.exception);
    }
  }

  @override
  Future<List<CommentModel>> getTitleComments(int titleId, {bool isTvShow = false}) async {
    final token = await getIt.get<UserRepositoryInterface>().getToken();
    final headers = {'Authorization': 'Bearer $token'};
    final uri = '$_baseUrl/${isTvShow ? 'tv' : 'movies'}/$titleId/comments';

    var response = await _service.getRequest(uri, headers: headers);
    if (response.sucess) {
      List<dynamic> data = response.content!['data'];
      return List<CommentModel>.from(data.map((json) => CommentModel.fromJson(json)));
    }

    return [];
  }

  @override
  Future<bool> removeComment(int titleId, int commentId, {bool isTvShow = false}) async {
    final token = await getIt.get<UserRepositoryInterface>().getToken();
    final headers = {'Authorization': 'Bearer $token'};
    final uri = '$_baseUrl/${isTvShow ? 'tv' : 'movies'}/$titleId/$commentId/comment';

    var response = await _service.deleteRequest(uri, headers: headers);

    if (response.sucess) {
      return response.sucess;
    }

    if (response.statusCode == 403) {
      throw CommentByOtherUserException();
    }

    throw Exception('Falha ao excluir comentário');
  }

  @override
  Future<bool> saveComment(int titleId, String comment, {bool isTvShow = false}) async {
    final token = await getIt.get<UserRepositoryInterface>().getToken();
    final headers = {'Authorization': 'Bearer $token'};
    final uri = '$_baseUrl/${isTvShow ? 'tv' : 'movies'}/$titleId/comment';

    var response = await _service.postRequest(uri, {'comment': comment}, headers: headers);
    return response.sucess;
  }

  @override
  Future<int> getTitleRate(int titleId, {bool isTvShow = false}) async {
    final token = await getIt.get<UserRepositoryInterface>().getToken();
    final headers = {'Authorization': 'Bearer $token'};
    final uri = '$_baseUrl/${isTvShow ? 'tv' : 'movies'}/$titleId/rate';

    var response = await _service.getRequest(uri, headers: headers);
    if (response.sucess) {
      return response.content!['rate'];
    }

    if (response.statusCode == 404) {
      throw TitleNotRatedException();
    }

    throw Exception('Falha ao cadastrar o usuário');
  }

  @override
  Future<bool> saveTitleRate(int titleId, int rate, {bool isTvShow = false}) async {
    final token = await getIt.get<UserRepositoryInterface>().getToken();
    final headers = {'Authorization': 'Bearer $token'};
    final uri = '$_baseUrl/${isTvShow ? 'tv' : 'movies'}/$titleId/rate';

    var response = await _service.postRequest(uri, {'rate': rate}, headers: headers);
    return response.sucess;
  }

  @override
  Future<List<TitleRatedModel>> getUserRatedTitleList(String? userId) async {
    final token = await getIt.get<UserRepositoryInterface>().getToken();
    final headers = {'Authorization': 'Bearer $token'};

    if (userId == null) {
      var userModel = await getIt.get<UserRepositoryInterface>().getUser();
      userId = userModel.id;
    }

    final uri = '$_baseUrl/users/$userId/titles-rated';

    var response = await _service.getRequest(uri, headers: headers);
    if (response.sucess) {
      List<dynamic> data = response.content!['data'];
      return List<TitleRatedModel>.from(data.map((json) => TitleRatedModel.fromJson(json)));
    }

    return [];
  }
}
