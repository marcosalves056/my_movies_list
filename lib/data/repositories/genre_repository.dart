import 'package:my_movies_list/data/models/genre_model.dart';
import 'package:my_movies_list/data/repositories/genre_repository_inteface.dart';
import 'package:my_movies_list/data/services/http_service.dart';

class GenreRepository implements GenreRepositoryInterface {
  final HttpService _service;
  final String _baseUrl = 'https://xbfuvqstcb.execute-api.us-east-1.amazonaws.com/dev';

  GenreRepository(this._service);

  @override
  Future<List<GenreModel>> getGenreList() async {
    String uri = '$_baseUrl/movies/';
    var response = await _service.getRequest(uri);

    if (response.sucess) {
      List<dynamic> data = response.content!['data'];
      return List<GenreModel>.from(data.map((json) => GenreModel.fromJson(json)));
    }

    return [];
  }
}
