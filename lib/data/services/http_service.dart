import 'dart:convert';

import 'package:http/http.dart' as http;

class Response {
  int statusCode;
  Map<String, dynamic>? content;
  Exception? exception;

  Response({required this.statusCode, this.content, this.exception});

  bool get sucess => statusCode == 200;
}

class HttpService {
  final _client = http.Client();

  Future<Response> getRequest(String uri, {Map<String, dynamic>? params, Map<String, String>? headers}) async {
    if (params != null) {
      uri += _buildQueryString(params);
    }

    var httpResponse = await _client.get(Uri.parse(uri), headers: headers);
    return _parseHttpResponse(httpResponse);
  }

  Future<Response> postRequest(String uri, Map<String, dynamic> body, {Map<String, String>? headers}) async {
    headers != null ? headers['Content-Type'] = 'application/json' : headers = {'Content-Type': 'application/json'};

    var httpResponse = await _client.post(Uri.parse(uri), body: jsonEncode(body), headers: headers);
    return _parseHttpResponse(httpResponse);
  }

  Future<Response> putRequest(String uri, Map<String, dynamic> body, {Map<String, String>? headers}) async {
    var httpResponse = await _client.put(Uri.parse(uri), body: jsonEncode(body));
    return _parseHttpResponse(httpResponse);
  }

  Future<Response> deleteRequest(String uri, {Map<String, String>? headers}) async {
    var httpResponse = await _client.delete(Uri.parse(uri), headers: headers);
    return _parseHttpResponse(httpResponse);
  }

  Response _parseHttpResponse(http.Response httpResponse) {
    Response response;

    try {
      response = Response(statusCode: httpResponse.statusCode, content: jsonDecode(httpResponse.body));
    } on Exception catch (e) {
      response = Response(statusCode: 500, exception: e);
    }

    return response;
  }

  String _buildQueryString(Map<String, dynamic> params) {
    var output = '?';
    params.forEach((key, value) => output += '$key=$value&');
    return output;
  }
}
